module gitlab.com/mtrchm/backend-logger

go 1.13

require (
	github.com/fatih/structs v1.1.0
	github.com/getsentry/sentry-go v0.5.0
	github.com/labstack/echo/v4 v4.1.14
	github.com/matrachma/go-logger-wrapper v0.0.0-20200205084916-907ddc0d79fd
)
