package logger

import (
	"context"
	"fmt"
	"github.com/fatih/structs"
	"github.com/getsentry/sentry-go"
	sentryecho "github.com/getsentry/sentry-go/echo"
	"github.com/labstack/echo/v4"
	lw "github.com/matrachma/go-logger-wrapper"
	"net/http"
	"os"
	"strings"
)

type Configurations struct {
	LoggerConfig LogConfigs
	SentryConfig SentryConfigs
}

type LogConfigs lw.Configuration

type SentryConfigs struct {
	SentryEnvMode    string
	SentryDSN        string
	SentryDebug      bool
	SentryStacktrace bool
}

type logData struct {
	EventLog  string `structs:"event_log"`
	Hostname  string `structs:"hostname"`
	ClientIP  string `structs:"client_ip"`
	ReqMethod string `structs:"req_method"`
	ReqURI    string `structs:"req_uri"`
	ReqID     string `structs:"req_id"`
	ResStat   int    `structs:"res_status"`
	ResSize   int64  `structs:"res_size"`
}

const (
	// Zap instance
	ZapLogger = lw.InstanceZapLogger
	// Logrus instance
	LogrusLogger = lw.InstanceLogrusLogger
	// Debug has verbose message
	Debug = lw.Debug
	// Info is default log level
	Info = lw.Info
	// Warn is for logging messages about possible issues
	Warn = lw.Warn
	// Error is for logging errors
	Error = lw.Error
	// Fatal is for logging fatal messages. The system shutdown after logging the message.
	Fatal = lw.Fatal
)

func Init(conf Configurations, loggerType int) error {
	if loggerType == ZapLogger {
		fmt.Print("Init zap instance logger")
	} else if loggerType == LogrusLogger {
		fmt.Print("Init logrus instance logger")
	}

	configLogger := lw.Configuration{
		EnableConsole:     conf.LoggerConfig.EnableConsole,
		EnableColor:       conf.LoggerConfig.EnableColor,
		ConsoleJSONFormat: conf.LoggerConfig.ConsoleJSONFormat,
		ConsoleLevel:      conf.LoggerConfig.ConsoleLevel,
		EnableFile:        conf.LoggerConfig.EnableFile,
		FileJSONFormat:    conf.LoggerConfig.FileJSONFormat,
		FileLevel:         conf.LoggerConfig.FileLevel,
		FileLocation:      conf.LoggerConfig.FileLocation,
	}

	err := lw.NewLogger(configLogger, loggerType)
	if err != nil {
		return err
	}

	err = sentry.Init(sentry.ClientOptions{
		Dsn:         conf.SentryConfig.SentryDSN,
		Environment: conf.SentryConfig.SentryEnvMode,
		BeforeSend: func(event *sentry.Event, hint *sentry.EventHint) *sentry.Event {
			if hint.Context != nil {
				if _, ok := hint.Context.Value(sentry.RequestContextKey).(*http.Request); ok {
					MakeLogEntry(nil, nil, lw.Info, "incoming request details")
				}
			}

			return event
		},
		Debug:            conf.SentryConfig.SentryDebug,
		AttachStacktrace: conf.SentryConfig.SentryStacktrace,
	})
	if err != nil {
		return err
	}

	return nil
}

func MakeLogEntry(ctx context.Context, err error, level string, format string, args ...interface{}) {
	var l lw.Logger
	var event string
	var c echo.Context
	msg := fmt.Sprintf(format, args...)
	hostname, _ := os.Hostname()
	if ctx != nil {
		event = ctx.Value("event").(string)
		if ctx.Value("echo.Context") != nil {
			c = ctx.Value("echo.Context").(echo.Context)
		} else {
			c = nil
		}
	} else {
		event = ""
		c = nil
	}

	if c == nil {
		l = lw.WithFields(lw.Fields{})
	} else {
		if event == "" || strings.Contains(event, "errorHandler") {
			if c.Response().Committed {
				l = lw.WithFields(structs.Map(logData{
					EventLog:  event,
					Hostname:  hostname,
					ClientIP:  c.RealIP(),
					ReqMethod: c.Request().Method,
					ReqURI:    c.Request().URL.String(),
					ReqID:     c.Request().Header.Get("X-REQUEST-ID"),
					ResStat:   c.Response().Status,
					ResSize:   c.Response().Size,
				}))
			} else {
				l = lw.WithFields(structs.Map(logData{
					EventLog:  event,
					Hostname:  hostname,
					ClientIP:  c.RealIP(),
					ReqMethod: c.Request().Method,
					ReqURI:    c.Request().URL.String(),
					ReqID:     c.Request().Header.Get("X-REQUEST-ID"),
					ResStat:   0,
					ResSize:   -1,
				}))
			}
		} else {
			if stringInSlice(level, []string{lw.Warn, lw.Error, lw.Fatal}) {
				if c.Response().Committed {
					l = lw.WithFields(structs.Map(logData{
						EventLog:  event,
						Hostname:  hostname,
						ClientIP:  c.RealIP(),
						ReqMethod: c.Request().Method,
						ReqURI:    c.Request().URL.String(),
						ReqID:     c.Request().Header.Get("X-REQUEST-ID"),
						ResStat:   c.Response().Status,
						ResSize:   c.Response().Size,
					}))
				} else {
					l = lw.WithFields(structs.Map(logData{
						EventLog:  event,
						Hostname:  hostname,
						ClientIP:  c.RealIP(),
						ReqMethod: c.Request().Method,
						ReqURI:    c.Request().URL.String(),
						ReqID:     c.Request().Header.Get("X-REQUEST-ID"),
						ResStat:   0,
						ResSize:   -1,
					}))
				}
			} else {
				if c.Response().Committed {
					l = lw.WithFields(lw.Fields{
						"event_log":  event,
						"request_id": c.Request().Header.Get("X-REQUEST-ID"),
					})
				} else {
					l = lw.WithFields(lw.Fields{
						"event_log":  event,
						"request_id": c.Request().Header.Get("X-REQUEST-ID"),
					})
				}
			}
		}
	}

	switch level {
	case lw.Debug:
		l.Debugf(msg)
	case lw.Info:
		l.Infof(msg)
	case lw.Warn:
		l.Warnf(msg)
	case lw.Error:
		l.Errorf(msg)
	case lw.Fatal:
		l.Fatalf(msg)
	default:
		l.Infof(msg)
	}

	if !stringInSlice(level, []string{lw.Info, lw.Debug, lw.Warn}) {
		logToSentry(c, level, err, msg)
	}
}

func stringInSlice(a string, strings []string) bool {
	for _, b := range strings {
		if b == a {
			return true
		}
	}
	return false
}

func logToSentry(c echo.Context, lvl string, err error, msg string) {
	if c != nil {
		if hub := sentryecho.GetHubFromContext(c); hub != nil {
			hub.WithScope(func(scope *sentry.Scope) {
				scope.SetLevel(sentry.Level(lvl))
				if err != nil {
					hub.CaptureException(err)
				} else {
					hub.CaptureMessage(msg)
				}
			})
		}
	} else {
		sentry.ConfigureScope(func(scope *sentry.Scope) {
			scope.SetLevel(sentry.Level(lvl))
			if err != nil {
				sentry.CaptureException(err)
			} else {
				sentry.CaptureMessage(msg)
			}
		})
	}
}
